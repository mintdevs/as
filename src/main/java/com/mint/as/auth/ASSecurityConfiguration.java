package com.mint.as.auth;

import com.mint.as.data.repository.OrganizationRepository;
import com.mint.as.rest.APIExceptionHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@EnableWebSecurity
public class ASSecurityConfiguration extends WebSecurityConfigurerAdapter {

    Logger logger = LogManager.getLogger(ASSecurityConfiguration.class);

    @Autowired
    AuthUserDetailService authUserDetailService;

    @Autowired
    RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    LogoutSuccessHandler logoutSuccessHandler;

    private SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler();


   public ASSecurityConfiguration(){
       SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
   }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("mint").password( encoder().encode("password")).roles(UserRole.ADMIN.name());

        auth.userDetailsService(authUserDetailService).passwordEncoder(encoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        logger.warn( "========== configure(HttpSecurity) ====");
        http
                .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/org/*/conformance").hasAnyRole(UserRole.ADMIN.name(), UserRole.CLIENT.name())
                .antMatchers(HttpMethod.GET,"/org/*/conformance/*").hasAnyRole(UserRole.ADMIN.name(), UserRole.CLIENT.name())
                .antMatchers(HttpMethod.POST,"/org/*/conformance").hasAnyRole(UserRole.ADMIN.name(), UserRole.CLIENT.name())
                .antMatchers("/**").hasRole(UserRole.ADMIN.name())
                .and().formLogin().successHandler(authenticationSuccessHandler).failureHandler(failureHandler)
                .and()
                .logout().logoutSuccessHandler( logoutSuccessHandler);
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }
}
