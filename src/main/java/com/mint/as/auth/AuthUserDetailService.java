package com.mint.as.auth;

import com.mint.as.data.model.Organization;
import com.mint.as.data.model.User;
import com.mint.as.data.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthUserDetailService implements UserDetailsService {

    @Autowired
    OrganizationRepository organizationRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Organization organization = organizationRepository.findUserByName(userName);
        if (organization == null) {
            throw new UsernameNotFoundException(userName);
        }

        User user = organization.getUsers().stream().filter( u -> { return u.getUserName().equals(userName);}).findFirst().get();
        if( user == null){
            throw new UsernameNotFoundException(userName);
        }
        return new AuthUserDetail(user);
    }
}
