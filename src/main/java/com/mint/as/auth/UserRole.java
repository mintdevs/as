package com.mint.as.auth;

public enum UserRole {
    ADMIN,
    CLIENT,
    GUEST
}
