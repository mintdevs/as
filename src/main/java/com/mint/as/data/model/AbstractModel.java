package com.mint.as.data.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
@Document
public abstract class AbstractModel {

    private Date updateTime;

    private String updateUser;

    public AbstractModel() {
        init();
    }

    private void init(){
        updateTime =  Date.from(ZonedDateTime.now(ZoneOffset.UTC).toInstant());
        updateUser = "mint";
    }
}
