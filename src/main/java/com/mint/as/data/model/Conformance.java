package com.mint.as.data.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Conformance extends AbstractModel{


    @Id
    @EqualsAndHashCode.Include
    private String caseNo;

    private String title;

    private boolean isMandatory;
}
