package com.mint.as.data.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Data
@Document
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ConformanceGroup extends AbstractModel{

    @Id
    @EqualsAndHashCode.Include
    private ConformanceType conformanceType;

    private Set<String> conformanceCases;

    public ConformanceGroup() {
        super();
        this.conformanceCases = new HashSet<>();
    }

    public ConformanceGroup(ConformanceType conformanceType) {
        this();
        this.conformanceType = conformanceType;
    }

    public enum ConformanceType {
        orders,
        streamingorders,
        lastlook,
        admin
    }
}
