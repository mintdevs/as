package com.mint.as.data.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ConformanceState {

    @EqualsAndHashCode.Include
    private String caseNo;

    private List<String> clientTestMessage;

    private String skipReason;

    private TestResult testResult;

    private String errorMessage;

    public ConformanceState() {
        testResult = new TestResult();
        clientTestMessage = new ArrayList<>();
    }

    public ConformanceState(String caseNo){
        this();
        this.caseNo = caseNo;
    }


}
