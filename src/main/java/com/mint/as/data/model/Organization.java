package com.mint.as.data.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Document
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Organization extends AbstractModel{

    @Id
    @EqualsAndHashCode.Include
    private String orgId;

    @NotEmpty( message = "orgName not be empty")
    @Indexed( unique = true)
    private String orgName;

    @NotNull( message = "conformanceGroup must not be empty")
    private ConformanceGroup.ConformanceType conformanceGroup;

    private List<ConformanceState> conformances;

    private List<User> users;

    public Organization() {
        super();
        this.conformances = new ArrayList<>();
        this.users = new ArrayList<>();
    }


    public Organization addConformance(String caseNo){
        this.conformances.add(new ConformanceState(caseNo));
        return this;
    }


    public Organization addConformances(Set<String> testCases){
        this.conformances.addAll(  testCases.stream().map( caseNo-> new ConformanceState( caseNo)).collect(Collectors.toList()));
        return this;
    }


}
