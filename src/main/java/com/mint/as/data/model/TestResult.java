package com.mint.as.data.model;

import lombok.Data;

@Data
public class TestResult {

    private TestState testState = TestState.UNINITIALIZED;
    private String reason="Not Tested Yet";

    public enum TestState{
        PASS,
        FAIL,
        UNINITIALIZED
    }

}
