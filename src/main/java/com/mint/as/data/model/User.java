package com.mint.as.data.model;

import com.mint.as.auth.UserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotEmpty;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User{

    @NotEmpty( message = "userName must be not empty")
    @EqualsAndHashCode.Include
    @Indexed( unique = true)
    private String userName;

    private String password;

    private UserRole userRole;

    private String firstName;
    private String lastName;
    private String email;

    public User() {
        super();
        this.userRole = UserRole.CLIENT;
    }


}
