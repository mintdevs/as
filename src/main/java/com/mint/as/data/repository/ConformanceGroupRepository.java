package com.mint.as.data.repository;

import com.mint.as.data.model.ConformanceGroup;
import com.mint.as.data.model.Organization;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ConformanceGroupRepository extends MongoRepository<ConformanceGroup,String> {
    @Query( "{'conformanceType':?0}")
    ConformanceGroup findByConformanceType(ConformanceGroup.ConformanceType conformanceType);
}
