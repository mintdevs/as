package com.mint.as.data.repository;

import com.mint.as.data.model.Conformance;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ConformanceRepository extends MongoRepository<Conformance,String> {
    @Query("{'caseNo':?0}")
    Conformance findByCaseNo(String caseNo);
}
