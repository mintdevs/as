package com.mint.as.data.repository;

import com.mint.as.data.model.Organization;
import com.mint.as.data.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface OrganizationRepository extends MongoRepository<Organization,String> {
    @Query("{'orgId':?0}")
    Organization findByOrgId( String orgId);

    @Query("{ 'users.userName':?0}")
    Organization findUserByName(String userName);
}
