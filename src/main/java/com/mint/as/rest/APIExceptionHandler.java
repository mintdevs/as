package com.mint.as.rest;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class APIExceptionHandler extends ResponseEntityExceptionHandler {

    Logger logger = LoggerFactory.getLogger(APIExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        logger.error("process exception: ", ex);
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ApiErrorResponse error = new ApiErrorResponse("Error", details);
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ApiErrorResponse error = new ApiErrorResponse("Invalid Request", details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();

        if( ex instanceof MethodArgumentTypeMismatchException) {
            details.add(String.format("%s %s is invalid.", ((MethodArgumentTypeMismatchException) ex).getName(), ex.getValue()));
            ApiErrorResponse error = new ApiErrorResponse("Invalid Request", details);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }else {
            return handleExceptionInternal(ex,null,headers,status,request);
        }
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();

        if( ex.getCause() instanceof InvalidFormatException ) {
            details.add(String.format("[%s] is invalid", ((InvalidFormatException) ex.getCause()).getValue()));
            ApiErrorResponse error = new ApiErrorResponse("Invalid Request", details);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }else {
            return handleExceptionInternal(ex,null,headers,status,request);
        }

    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("process internal exception: ", ex);
        List<String> details = new ArrayList<>();
        details.add(ex.getCause()!=null?ex.getCause().getLocalizedMessage():ex.getLocalizedMessage());
        ApiErrorResponse error = new ApiErrorResponse("Error", details);
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
