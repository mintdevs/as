package com.mint.as.rest;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class ApiErrorResponse {

    private String message;
    private List<String> details;

    private ApiErrorResponse() {
        details = Collections.emptyList();
    }


    ApiErrorResponse(String message, List<String> details) {
        this();
        this.message = message;
        this.details = details;
    }

}
