package com.mint.as.rest.controller;

import com.mint.as.data.model.Conformance;
import com.mint.as.data.repository.ConformanceRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/conformance")
public class ConformanceController {

    private static final Logger logger = LogManager.getLogger(ConformanceController.class);

    @Autowired
    private ConformanceRepository conformanceRepository;


    @RequestMapping(method = RequestMethod.GET)
    public List<Conformance> getAll(){return conformanceRepository.findAll();}

    @RequestMapping(path = "/{caseNo}", method = RequestMethod.GET)
    public Conformance getByCaseNo( @PathVariable("caseNo") String caseNo){return conformanceRepository.findByCaseNo(caseNo);}

    @RequestMapping(method = RequestMethod.POST)
    public Conformance createOrUpdate( @Valid @RequestBody Conformance conformance){ return conformanceRepository.save(conformance);}

    @RequestMapping(path="/{caseNo}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("caseNo") String caseNo) throws Exception{
        Conformance existing = conformanceRepository.findByCaseNo(caseNo);
        if(existing==null){
            throw new Exception( String.format("Case: %s doesn't exist",caseNo));
        }
        conformanceRepository.delete(existing);
    }
}
