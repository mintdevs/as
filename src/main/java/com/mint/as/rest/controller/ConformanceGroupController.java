package com.mint.as.rest.controller;

import com.mint.as.data.model.ConformanceGroup;
import com.mint.as.data.repository.ConformanceGroupRepository;
import com.mint.as.data.repository.ConformanceRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/conformancegroup")
public class ConformanceGroupController {

    Logger logger = LogManager.getLogger(ConformanceGroupController.class);

    @Autowired
    ConformanceGroupRepository conformanceGroupRepository;

    @Autowired
    ConformanceRepository conformanceRepository;

    @RequestMapping( method = RequestMethod.GET)
    public List<ConformanceGroup> getAll( ){return conformanceGroupRepository.findAll();}

    @RequestMapping( path="/{conformanceType}",method = RequestMethod.GET)
    public Set<String> getConformancesByType(@PathVariable("conformanceType") String  conformanceType )throws Exception {
        return validateConformanceType(validateConformanceTypeName(conformanceType)).getConformanceCases();
    }

    @RequestMapping( path="/{conformanceType}", method = RequestMethod.POST)
    public void createOrUpdate(@NotNull @PathVariable("conformanceType" ) String conformanceType) throws Exception{
        conformanceGroupRepository.save(new ConformanceGroup(validateConformanceTypeName(conformanceType)));
    }


    @RequestMapping( path="/{conformanceType}/{caseNo}", method = RequestMethod.POST)
    public void createOrUpdate(@NotNull @PathVariable("conformanceType" ) String conformanceType, @NotNull @PathVariable("caseNo") String caseNo) throws Exception{
        validateTestCases( caseNo );
        ConformanceGroup conformanceGroup = validateConformanceType(validateConformanceTypeName(conformanceType));

        conformanceGroup.getConformanceCases().add( caseNo);
        conformanceGroupRepository.save(conformanceGroup);
    }

    @RequestMapping(path="/{conformanceType}/{caseNo}", method = RequestMethod.DELETE)
    public void delete(@NotNull @PathVariable("conformanceType" ) String conformanceType, @NotNull @PathVariable("caseNo") String caseNo)  throws Exception{
        validateTestCases( caseNo );
        ConformanceGroup conformanceGroup = validateConformanceType(validateConformanceTypeName(conformanceType));

        conformanceGroup.getConformanceCases().remove( caseNo);
        conformanceGroupRepository.save(conformanceGroup);
    }

    private void validateTestCases(String  caseNo) throws Exception{
        if( conformanceRepository.findByCaseNo(caseNo)==null){
            throw new Exception(String.format("invalid case no: [%s]",caseNo));
        }
    }

    private ConformanceGroup.ConformanceType validateConformanceTypeName(String conformanceTypeName) throws Exception {
        try {
            ConformanceGroup.ConformanceType conformanceType = ConformanceGroup.ConformanceType.valueOf(conformanceTypeName);
            return conformanceType;
        } catch ( IllegalArgumentException e){
            throw new Exception(String.format("Invalid Conformance Group: [%s]. Valid values:[%s]", conformanceTypeName,
                    Arrays.asList( ConformanceGroup.ConformanceType.values()).stream().map( ConformanceGroup.ConformanceType::name ).collect( Collectors.joining(",") )));
        }
    }

    private ConformanceGroup validateConformanceType(ConformanceGroup.ConformanceType conformanceType) throws Exception {
        ConformanceGroup conformanceGroup = conformanceGroupRepository.findByConformanceType(conformanceType);
        if (conformanceGroup == null) {
            throw new Exception(String.format(" Conformance Group: %s doesn't exist", conformanceGroup));
        }
        return conformanceGroup;
    }
}
