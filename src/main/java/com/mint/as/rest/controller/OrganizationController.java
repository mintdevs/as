package com.mint.as.rest.controller;


import com.mint.as.auth.UserRole;
import com.mint.as.data.model.ConformanceState;
import com.mint.as.data.model.Organization;
import com.mint.as.data.model.User;
import com.mint.as.data.repository.ConformanceRepository;
import com.mint.as.data.repository.ConformanceGroupRepository;
import com.mint.as.data.repository.OrganizationRepository;
import com.mint.as.data.model.ConformanceGroup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/org")
public class OrganizationController {

    private static final Logger logger = LogManager.getLogger(OrganizationController.class);

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private ConformanceRepository conformanceRepository;

    @Autowired
    private ConformanceGroupRepository conformanceGroupRepository;

    @Autowired
    PasswordEncoder encoder;


    @RequestMapping(method = RequestMethod.GET)
    public List<String> getAll(){
        return organizationRepository.findAll().stream().map(Organization::getOrgId).collect( Collectors.toList());
    }

    @RequestMapping(path = "/{orgId}", method = RequestMethod.GET)
    public Organization getOrganizationByOrgId( @PathVariable("orgId") String orgId) throws Exception{
         return validateOrgId(orgId);
    }

    @RequestMapping(path = "/{orgId}/conformance", method = RequestMethod.GET)
    public List<ConformanceState> getConformancesByOrgId(@PathVariable("orgId") String orgId) throws Exception{
        return validateOrgId(orgId).getConformances();
    }

    @RequestMapping(path = "/{orgId}/conformance/{caseNo}", method = RequestMethod.GET)
    public ConformanceState getConformanceByCaseNo(@PathVariable("orgId") String orgId,  @NotNull @PathVariable("caseNo") String caseNo) throws Exception{
        Organization org = validateOrgId(orgId);
        validateCase( caseNo);

        ConformanceState cstate =  org.getConformances().stream().filter(  conformanceState -> {  return conformanceState.getCaseNo().equals(caseNo);}).findFirst().get();
        if( cstate==null){
            throw new Exception( "conformance case doesn't exist in the org" );
        }

        return cstate;
    }

    @RequestMapping(path = "/{orgId}/user", method = RequestMethod.GET)
    public List<String> getUsersByOrgId(@PathVariable("orgId") String orgId) throws Exception{
        return validateOrgId(orgId).getUsers().stream().map(  User::getUserName).collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    public Organization createOrUpdate(@Valid @RequestBody Organization organization) throws Exception{
        validate( organization);
        ConformanceGroup conformanceGroup = conformanceGroupRepository.findByConformanceType( organization.getConformanceGroup());
        if( conformanceGroup != null) // Note: even though we have validated to check if the conformance group is valid, but if it has not been added (yet) - the conformanceGroup does not exist in db
            organization.addConformances(conformanceGroupRepository.findByConformanceType( organization.getConformanceGroup()).getConformanceCases());
        return organizationRepository.save(organization);
    }

    @RequestMapping(path = "/{orgId}/conformance/{caseNo}",method = RequestMethod.POST)
    public Organization updateConformance(@NotNull @PathVariable( "orgId") String orgId, @NotNull @PathVariable( "caseNo") String caseNo) throws Exception{
        Organization org = validateOrgId( orgId);
        validateCase(caseNo);

        if( !org.getConformances().stream().filter(  conformanceState -> conformanceState.getCaseNo().equals(caseNo)).findAny().isPresent()){
            org.addConformance(caseNo);
        }
        return organizationRepository.save(org);
    }

    @RequestMapping(path = "/{orgId}/user",method = RequestMethod.POST)
    public Organization createOrUpdateUser(@NotNull @PathVariable( "orgId") String orgId, @Valid @RequestBody User user) throws Exception{
        Organization org = validateOrgId( orgId);

        Optional<User>  userOpt = org.getUsers().stream().filter(u -> u.getUserName().equals(user.getUserName())).findAny();
        if( userOpt.isPresent()) {
            updateUser(userOpt.get(), user);
        }else {
            user.setUserRole( org.getConformanceGroup() == ConformanceGroup.ConformanceType.admin? UserRole.ADMIN:UserRole.CLIENT);
            user.setPassword(encoder.encode(user.getPassword()));
            org.getUsers().add(user);
        }

        return organizationRepository.save(org);
    }


    @RequestMapping(path = "/{orgId}/conformance",method = RequestMethod.POST)
    public Organization submitConformance(@NotNull @PathVariable( "orgId") String orgId, @Valid @RequestBody List<ConformanceState> submitted) throws Exception{
        Organization org = validateOrgId( orgId);
        Map<String, ConformanceState>  currentConformances = org.getConformances().stream().collect( Collectors.toMap( ConformanceState::getCaseNo, conformanceState ->  conformanceState));

        Set<String> newTestCases = submitted.stream().map(  ConformanceState::getCaseNo ).collect(Collectors.toSet());
        newTestCases.removeAll( currentConformances.keySet() );
        if( newTestCases.size()>0){
            throw new Exception( String.format("Test Cases: %s are not included in the org %s", newTestCases.stream().collect(Collectors.joining(",") ) ,orgId ));
        }

        submitted.stream().forEach( newState -> {
            ConformanceState currentConformance = currentConformances.get(newState.getCaseNo());
            update(currentConformance,  newState);

        } );

        return organizationRepository.save(org);
    }


    @RequestMapping(path="/{orgId}",method = RequestMethod.DELETE)
    public void deleteByOrgId( @NotNull @PathVariable("orgId") String orgId) throws Exception{
        Organization org = validateOrgId(orgId);
        organizationRepository.delete(org);
    }

    @RequestMapping(path="/{orgId}/conformance",method = RequestMethod.DELETE)
    public void deleteConformancesByOrgId( @NotNull @PathVariable("orgId") String orgId) throws Exception{
        Organization org = validateOrgId(orgId);
        org.getConformances().clear();
        organizationRepository.save(org);
    }

    @RequestMapping(path="/{orgId}/conformance/{caseNo}",method = RequestMethod.DELETE)
    public void deleteConformancesByCaseNo( @NotNull @PathVariable("orgId") String orgId,@NotNull @PathVariable("caseNo") String caseNo) throws Exception{
        Organization org = validateOrgId(orgId);

        Optional<ConformanceState> stateOptional = org.getConformances().stream().filter(  conformanceState ->conformanceState.getCaseNo().equals(caseNo)).findAny();
        if( stateOptional.isPresent()) {
            org.getConformances().remove(stateOptional.get());
            organizationRepository.save(org);
        }
    }

    @RequestMapping(path="/{orgId}/user",method = RequestMethod.DELETE)
    public void deleteUserByOrgId( @NotNull @PathVariable("orgId") String orgId) throws Exception{
        Organization org = validateOrgId(orgId);
        org.getUsers().clear();
        organizationRepository.save(org);
    }


    @RequestMapping(path="/{orgId}/user/{userName}",method = RequestMethod.DELETE)
    public void deleteUserByName( @NotNull @PathVariable("orgId") String orgId,@NotNull @PathVariable("userName") String userName) throws Exception{
        Organization org = validateOrgId(orgId);

        Optional<User> userOpt = org.getUsers().stream().filter( u ->u.getUserName().equals(userName)).findAny();
        if( userOpt.isPresent()) {
            org.getUsers().remove(userOpt.get());
            organizationRepository.save(org);
        }
    }


    private void validate(Organization organization) throws Exception {
        List<String> nonEixstingCases = new ArrayList<>();
        organization.getConformances().stream().forEach(   conformanceState -> {
            try {
                validateCase( conformanceState.getCaseNo());
            } catch (Exception e) {
                nonEixstingCases.add( conformanceState.getCaseNo());
            }
        });

        if( nonEixstingCases.size()>0){
            throw new Exception( String.format( "invalid conformance cases[ %s] ", nonEixstingCases.stream().collect( Collectors.joining(",") )));
        }
    }

    private Organization validateOrgId(String orgId) throws Exception {

        Organization org = organizationRepository.findByOrgId(orgId);
        if( org == null) {
            throw new Exception( String.format("orgId: %s doesn't exist",orgId ));
        }
        return org;
    }


    private void validateCase(String caseNo) throws Exception{
        if( conformanceRepository.findByCaseNo(caseNo) == null){
            throw new Exception(String.format("invalid case no: [%s]",caseNo));
        }
    }

    private void update(ConformanceState currentConformance, ConformanceState newState) {
        currentConformance.setSkipReason( newState.getSkipReason());
        currentConformance.setClientTestMessage( newState.getClientTestMessage() );
    }

    private void updateUser(User currentUser, User updatedUser) {
        if( updatedUser.getPassword()!=null){
            currentUser.setPassword( encoder.encode(updatedUser.getPassword()) );
        }

        if( updatedUser.getEmail()!=null){
            currentUser.setEmail( updatedUser.getEmail());
        }

        if( updatedUser.getFirstName()!=null){
            currentUser.setFirstName(updatedUser.getFirstName());
        }

        if( updatedUser.getLastName()!=null){
            currentUser.setLastName( updatedUser.getLastName());
        }
    }

}
